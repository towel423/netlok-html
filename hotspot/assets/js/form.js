console.clear();

const loginBtn = document.getElementById('xlogin');
const signupBtn = document.getElementById('xvoucher');

loginBtn.addEventListener('click', (e) => {
	let parent = e.target.parentNode.parentNode;
	Array.from(e.target.parentNode.parentNode.classList).find((element) => {
		if(element !== "xslide-up") {
			parent.classList.add('xslide-up')
		}else{
			signupBtn.parentNode.classList.add('xslide-up')
			parent.classList.remove('xslide-up')
		}
	});
});

signupBtn.addEventListener('click', (e) => {
	let parent = e.target.parentNode;
	Array.from(e.target.parentNode.classList).find((element) => {
		if(element !== "xslide-up") {
			parent.classList.add('xslide-up')
		}else{
			loginBtn.parentNode.parentNode.classList.add('xslide-up')
			parent.classList.remove('xslide-up')
		}
	});
});