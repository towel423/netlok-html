window.onload = function() {
	uptime();	
	timeleft();
	};
	
function uptime()
{
	var uptime = document.getElementById("uptime").innerHTML;
	var d="",h="",m="",s="";
	var hari="", jam="", menit="", detik="" ;
	
	if(uptime.indexOf('d') !== -1){
		d = uptime.slice(0,uptime.indexOf('d'));
		uptime = uptime.slice(uptime.indexOf('d')+1);
		}
	if(uptime.indexOf('h')!== -1){
		h = uptime.slice(0,uptime.indexOf('h'));
		uptime = uptime.slice(uptime.indexOf('h')+1);
		}
	if(uptime.indexOf('m')!== -1){
		m = uptime.slice(0,uptime.indexOf('m'));
		uptime = uptime.slice(uptime.indexOf('m')+1);
		}
	if(uptime.indexOf('s')!== -1){
		s = uptime.slice(0,uptime.indexOf('s'));
    }
    
    if(d != ""){
		  hari = d + " <small>Hari</small> | ";
		}
		if(h != ""){
		  jam = h + " <small>Jam</small> | ";
		}
		if(m != ""){
		  menit = m + " <small>Menit</small> | ";
		}
		if(s != ""){
		  detik = s + " <small>Detik</small>";
		}
		
		document.getElementById("uptime").innerHTML = hari+jam+menit+detik;
}

function timeleft()
{
  var timeleft = document.getElementById("timeleft").innerHTML;
	var d="",h="",m="",s="";
	var hari="", jam="", menit="", detik="" ;
	
	if(timeleft.indexOf('d') !== -1){
		d = timeleft.slice(0,timeleft.indexOf('d'));
		timeleft = timeleft.slice(timeleft.indexOf('d')+1);
		}
	if(timeleft.indexOf('h')!== -1){
		h = timeleft.slice(0,timeleft.indexOf('h'));
		timeleft = timeleft.slice(timeleft.indexOf('h')+1);
		}
	if(timeleft.indexOf('m')!== -1){
		m = timeleft.slice(0,timeleft.indexOf('m'));
		timeleft = timeleft.slice(timeleft.indexOf('m')+1);
		}
	if(timeleft.indexOf('s')!== -1){
		s = timeleft.slice(0,timeleft.indexOf('s'));
    }
    
    if(d != ""){
		  hari = d + " <small>Hari</small> | ";
		}
		if(h != ""){
		  jam = h + " <small>Jam</small> | ";
		}
		if(m != ""){
		  menit = m + " <small>Menit</small> | ";
		}
		if(s != ""){
		  detik = s + " <small>Detik</small>";
		}
		
		document.getElementById("timeleft").innerHTML = hari+jam+menit+detik;
}